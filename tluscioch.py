#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import sqlite3
import os
import sys
import datetime
import matplotlib
matplotlib.rcParams['lines.markersize'] = 2
import matplotlib.pylab as plt
import numpy as np
from scipy import stats
from matplotlib import dates

# setup ########################################################################
your_height = 176 # cm
weight_goal = 68  # kg
size1_goal =  82
cols = 5 # number of columns in txt printing data
################################################################################

plt.style.use('ggplot')
# bmi
height = your_height/100.0 # meters

def print_fmt(arr, cols=5): # print multicol
    l = len(arr)
    rows = l//(cols-1) if l%cols != 0 else l//cols # one more column for not full col
    for i in range(rows):
        r = ["%-32s" % (arr[j*rows+i]) for j in range(cols) if l > j*rows+i]
        print "".join(r)

parser = argparse.ArgumentParser(description='Save weight, distances and sizes',
        prefix_chars='-')
parser.add_argument('-w', '--weight', type=float, nargs=1)
parser.add_argument('-s', '--size', type=float, nargs=1)
parser.add_argument('-d', '--distance', type=float, nargs=1)
parser.add_argument('--date', required=False,
                    help='Optional. Format YYYY/MM/DD. Script will use current \
                          date by default.')

inp = parser.parse_args()

# check if database exists
database = os.path.expanduser('~/.tluscioch.db')
if not os.path.isfile(database):
    conn = sqlite3.connect(database)
    curs = conn.cursor()
    curs.execute('CREATE TABLE stats (day date primary key not null unique, \
                  weight float, size1 float, distance float)')
    conn.commit()
    conn.close()

conn = sqlite3.connect(database)
curs = conn.cursor()
# insert values
if inp.date:
    pd = map(int, inp.date.split("/"))
    past_date = datetime.date(*pd)
    if curs.execute("SELECT count(*) FROM stats WHERE day=?",
                    [past_date]).fetchone()[0] == 0:
        curs.execute("INSERT INTO stats(day) VALUES(?)", [past_date])
    if inp.weight:
        curs.execute("UPDATE stats SET weight=? WHERE day=?",
                     [inp.weight[0], past_date])
    if inp.size:
        curs.execute("UPDATE stats SET size1=? WHERE day=?",
                     [inp.size[0], past_date])
    if inp.distance:
        curs.execute("UPDATE stats SET distance=? WHERE day=?",
                     [inp.distance[0], past_date])
elif inp.weight or inp.size or inp.distance: # today
    if curs.execute("SELECT count(*) FROM stats WHERE \
                     day=strftime('%Y-%m-%d','now')").fetchone()[0] == 0:
        curs.execute("INSERT INTO stats(day) VALUES(strftime('%Y-%m-%d','now'))")
    if inp.weight:
        curs.execute("UPDATE stats SET weight=? WHERE \
                      day=strftime('%Y-%m-%d','now');", inp.weight)
    if inp.size:
        curs.execute("UPDATE stats SET size1=? WHERE \
                      day=strftime('%Y-%m-%d','now');", inp.size)
    if inp.distance:
        curs.execute("UPDATE stats SET distance=? WHERE \
                      day=strftime('%Y-%m-%d','now');", inp.distance)
conn.commit()

data = curs.execute("SELECT day, weight, size1, distance \
        FROM stats ORDER BY day").fetchall()
conn.close()
if len(data) == 0:
    sys.exit(0)

# plotting
data = np.array(data)
day = data[:, 0]
weight = np.array(data[:, 1], dtype=np.float)
size1 = np.array(data[:, 2], dtype=np.float)
distance = np.array(data[:, 3], dtype=np.float)
distance2 = np.array(data[:, 3], dtype=np.float)

# print txt to console
print_fmt(data, cols)
print("Weight  #  avg %5.1f min %5.1f max %5.1f"
        %(np.nanmean(weight), np.nanmin(weight), np.nanmax(weight)))
print("Tail    #  avg %5.1f min %5.1f max %5.1f"
        %(np.nanmean(size1), np.nanmin(size1), np.nanmax(size1)))
print("Distance#  avg %5.1f min %5.1f max %5.1f"
        %(np.nanmean(distance), np.nanmin(distance), np.nanmax(distance)))


for i in range(1,distance.shape[0]):
    if not np.isnan(distance[i]):
        distance[i] +=  distance[i-1]
    else:
        distance[i] = distance[i-1]
if np.isnan(distance[0]):
    distance[0] = distance[1]

new_x = map(datetime.datetime.strptime, day, len(day)*['%Y-%m-%d'])
fig, ax = plt.subplots(3, sharex=True)
myFmt = matplotlib.dates.DateFormatter('%d-%m')
ax[0].xaxis.set_major_formatter(myFmt)

for i in range(1, weight.shape[0]):
    if np.isnan(weight[i]):
        weight[i] = weight[i-1]
    if np.isnan(size1[i]):
        size1[i] = size1[i-1]

if np.isnan(size1[0]):
    size1[0] = size1[1]
if np.isnan(weight[0]):
    weight[0] = weight[1]

ax4 = ax[0].twinx()
ax[0].grid(False)
ax[0].plot(new_x, weight/(height*height), linestyle=':', linewidth=0.5, marker='')
ax[0].yaxis.tick_left()
ax[0].set_ylabel('BMI')

ax4.plot_date(new_x, weight, linestyle='-', marker='o', linewidth=0.6)
ax4.set_title('Weight (min: %5.1f med: %5.1f, curr: %5.1f)'
        % (np.nanmin(weight), np.nanmedian(weight), weight[-1]))
ax4.fill_between(new_x, weight_goal, weight, alpha=0.3)
ax4.grid(True)
ax4.yaxis.tick_right()

ax[1].plot_date(new_x, size1, linestyle='-', marker='o', linewidth=0.6)
ax[1].set_title('Size1 (min: %5.1f med: %5.1f, curr: %5.1f)'
        % (np.nanmin(size1), np.nanmedian(size1), size1[-1]))
ax[1].grid(True)
ax[1].yaxis.tick_right()
ax[1].fill_between(new_x, size1_goal, size1, alpha=0.3)

ax[2].plot_date(new_x, distance, marker='')
ax[2].fill_between(new_x, 0, distance, alpha=0.3)
ax[2].set_title('Distance (sum: %5.1f med: %5.1f, last week: %5.1f)'
        % (np.nansum(distance2), np.nanmedian(distance2), np.nansum(distance2[-7:])))
ax[2].grid(True)
ax[2].yaxis.tick_right()
ax[2].set_ylabel('Total distance')

# extremas
min_weight = np.where(weight == weight.min())[0]
min_size1 = np.where(size1 == size1.min())[0]

# goals
p_goal_weight = np.where(weight <= weight_goal)[0]
p_goal_size1 = np.where(size1 <= size1_goal)[0]
new_x = np.array(new_x)
size1 = np.array(size1)
weight = np.array(weight)

ax4.plot_date(new_x[min_weight], weight[min_weight], marker='o', markersize=9,
        color='#FAA43A', markeredgecolor='#4D4D4D', markeredgewidth=0.3, alpha=0.5)
ax[1].plot_date(new_x[min_size1], size1[min_size1], marker='o', markersize=9,
        color='#FAA43A', markeredgecolor='#4D4D4D', markeredgewidth=0.3, alpha=0.5)

ax4.plot_date(new_x[p_goal_weight], weight[p_goal_weight], marker='o',
        markersize=4, color='#5DA5DA')
ax[1].plot_date(new_x[p_goal_size1], size1[p_goal_size1], marker='o',
        markersize=4, color='#5DA5DA')

# goals
ax4.plot(new_x, [weight_goal for i in range(len(new_x))], linestyle='-.', linewidth=0.9)
ax[1].plot(new_x, [size1_goal for i in range(len(new_x))], linestyle='-.', linewidth=0.9)
if p_goal_weight.shape[0]>0:
    t1 = dates.date2num(new_x[p_goal_weight[0]])-20
    delta = new_x[p_goal_weight[0]] - new_x[0]
    ax4.annotate('Achieved in %2d days. Cool!' % (delta.days),
            xytext=(dates.num2date(t1), weight_goal+4),
            arrowprops=dict(width=1,facecolor='red', shrink=0.1),
            xy=(new_x[p_goal_weight[0]], weight_goal), size=10)

if p_goal_size1.shape[0]>0:
    t1 = dates.date2num(new_x[p_goal_size1[0]])-20
    delta = new_x[p_goal_size1[0]] - new_x[0]
    ax[1].annotate('Achieved in %2d days. Cool!' % (delta.days),
            xytext=(dates.num2date(t1), size1_goal+2),
            arrowprops=dict(width=1,facecolor='red', shrink=0.1),
            xy=(new_x[p_goal_size1[0]], size1_goal), size=10)


ax3 = ax[2].twinx()
ax3.grid(False)
ax3.set_ylabel('Single trip distance')
ax3.bar(new_x, distance2, 0.4, edgecolor='#FAA43A', alpha=0.9, color='#FAA43A')
for i in range(distance2.shape[0]):
    if not np.isnan(distance2[i]):
        ax3.annotate("%.0f" %(distance2[i]), xy=(new_x[i],distance2[i]), size=8)


#o = [a for a in distance2 if not np.isnan(a)]
#n, bins, patches = ax[3].hist(weight, 15, normed=0, histtype='stepfilled')


print stats.pearsonr(weight[1:], size1[1:])
plt.show()
