# Tluscioch

Diary for weight, some size and distance travelled by bike. By default it display plot value vs date.


**required**: matplotlib, sqlite

![screenshot](https://bitbucket.org/mjamroz/tluscioch/raw/master/screen.png)


## Options
~~~~
./tluscioch.py --help
usage: tluscioch.py [-h] [-w WEIGHT] [-s SIZE] [-d DISTANCE] [--date DATE]

Save weight, km and sizes

optional arguments:
  -h, --help            show this help message and exit
  -w WEIGHT, --weight WEIGHT
  -s SIZE, --size SIZE
  -d DISTANCE, --distance DISTANCE
  --date DATE           Optional. Format YYYY/MM/DD. Script will use current
                        date by default.
~~~~


**Author** Michal Jamroz
**Licence** public domain
**Dedication** To my belly

